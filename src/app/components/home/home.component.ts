import { Component, OnInit } from '@angular/core';
import { Person } from 'src/app/models/person';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public maVariable: string;
  public maVariable2: string;
  public maVariable3: number;
  public open: boolean;
  public list: Person[];

  constructor() { }

  ngOnInit() {
    this.list = [
      { name: 'Khun', birthDate: new Date(1982,5,6), job: 'Dev' },
      { name: 'Samir', birthDate: new Date(1989,1,1) },
      { name: 'Thierry', birthDate: new Date(1900,1,1), job: 'Killer' },
    ];
    this.open = false;
    this.maVariable3 = 0;
    this.maVariable2 = "machin";
    this.maVariable = "Khun";
    setTimeout(() => {
      this.maVariable = "Thierry"
    }, 3000);

    // setTimeout(function(){
    //   this.maVariable = "Thierry"
    // }, 3000);
  }

  toggle() {
    this.open = !this.open;
  }

  onClick() {
    if(this.maVariable3 < 10){
      this.maVariable3++;
    }
  }

}
