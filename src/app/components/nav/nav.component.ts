import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  public items: NbMenuItem[];  

  constructor() { }

  ngOnInit() {
    this.items = [
      { link: '/', title : 'Home', icon: 'home' },
      { link: '/about', title : 'About', icon: 'question-mark' },
      { link: '/shopping', title : 'Shopping List', icon: 'shopping-cart' },
    ];
  }

}
