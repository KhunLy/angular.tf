import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.scss']
})
export class ShoppingComponent implements OnInit {

  public articleName: string;
  public articles : Article[];

  constructor() { }

  ngOnInit() {
    this.articles = [];
  }

  add() {
    if(!this.articleName) return;
    let art = this.articles.find(a => 
      a.name.toLowerCase() == this.articleName.toLowerCase()
    );
    if(art){
      art.quantity++;
    }
    else{
      this.articles.push({ 
        name: this.articleName,
        checked: false ,
        quantity: 1
      });
    }
    this.articleName = null;
  }

  check(a: Article) {
    a.checked = !a.checked;
  }

}
