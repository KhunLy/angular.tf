export interface Article {
    name: string;
    checked: boolean;
    quantity: number;
}