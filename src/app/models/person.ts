export interface Person {
    name : string;
    job?: string;
    birthDate: Date;
}